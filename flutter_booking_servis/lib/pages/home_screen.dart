import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/controller/BannerController.dart';
import 'package:flutter_booking_servis/controller/NewsController.dart';
import 'package:flutter_booking_servis/network/base_url.dart';
import 'package:flutter_booking_servis/pages/detail_banner.dart';
import 'package:flutter_booking_servis/pages/detail_news_screen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'list_news_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {

    final NewsController newsController = Provider.of<NewsController>(context,listen: true);
    newsController.fetchNews();

    final NewsController newsControllerSlider = Provider.of<NewsController>(context,listen: true);
    newsControllerSlider.fetchNewsSlider();

    final BannerController bannerController =
        Provider.of<BannerController>(context,listen: true);
    bannerController.fetchBanner();

    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            bannerController.listBanner?.length != null
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 10, top: 10, bottom: 5),
                        child: Text(
                          'Banner',
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  )
                : Text(''),
            bannerController.listBanner?.length != null
                ? Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: SizedBox(
                      // height: 300,
                      height: MediaQuery.of(context).size.height * 0.264,
                      child: ListView.builder(
                        physics: ClampingScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: bannerController.listBanner.length,
                        itemBuilder: (BuildContext context, int index) =>
                            Container(
                          // width: 315,
                          // height: MediaQuery.of(context).size.height * 0.1,
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: InkWell(
                            onTap: () {
                              // print('click 2');
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailBannerScreen(
                                          judul: bannerController
                                              .listBanner[index].judul,
                                          isi: bannerController
                                              .listBanner[index].isi,
                                          gambar: bannerController
                                              .listBanner[index].gambar,
                                          tgl_post: bannerController
                                              .listBanner[index].tgl_post,
                                        )),
                              );
                            },
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              elevation: 4,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2)),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      // print('click 1');
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailBannerScreen(
                                                  judul: bannerController
                                                      .listBanner[index].judul,
                                                  isi: bannerController
                                                      .listBanner[index].isi,
                                                  gambar: bannerController
                                                      .listBanner[index].gambar,
                                                  tgl_post: bannerController
                                                      .listBanner[index]
                                                      .tgl_post,
                                                )),
                                      );
                                    },
                                    child: Stack(
                                      alignment: Alignment.bottomLeft,
                                      children: [
                                        Ink.image(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.25,
                                          // height:200,
                                          image: NetworkImage(Api.baseUrlImage +
                                              '${bannerController.listBanner[index].gambar}'),
                                          fit: BoxFit.fitWidth,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : Text(''),

            newsController.listnews?.length != null
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 10, top: 10, bottom: 5),
                        child: Text(
                          'Berita Terbaru',
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w500),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 10, top: 10, bottom: 5, right: 10),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ListNewsScreen()),
                            );
                          },
                          child: Text(
                            'Tampilkan Lebih',
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                        ),
                      ),
                    ],
                  )
                : Center(child: Text('')),
            // Judul Berita

            newsControllerSlider.listnewsSlider?.length != null
                // Berita Slider
                ? Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: SizedBox(
                      // height: 300,
                      height: MediaQuery.of(context).size.height * 0.5,
                      child: ListView.builder(
                        physics: ClampingScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: newsControllerSlider.listnewsSlider.length,
                        itemBuilder: (BuildContext context, int index) =>
                            Container(
                          // width: 315,
                          // height: MediaQuery.of(context).size.height * 0.1,
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: InkWell(
                            onTap: () {
                              // print('click 2');
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailNewsScreen()),
                              );
                            },
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              elevation: 4,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2)),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      // print('click 1');
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailNewsScreen(
                                                  judul: newsControllerSlider.listnewsSlider[index].judul,
                                                  isi: newsControllerSlider.listnewsSlider[index].isi,
                                                  gambar: newsControllerSlider.listnewsSlider[index].gambar,
                                                  highlights: newsControllerSlider.listnewsSlider[index]
                                                      .highlights,
                                                  tgl_post: newsControllerSlider.listnewsSlider[index].tgl_post,
                                                )),
                                      );
                                    },
                                    child: Stack(
                                      alignment: Alignment.bottomLeft,
                                      children: [
                                        Ink.image(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.25,
                                          // height:200,
                                          image: NetworkImage(Api.baseUrlImage +
                                              '${newsControllerSlider.listnewsSlider[index].gambar}'),
                                          fit: BoxFit.fitWidth,
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.03,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      print('click judul');
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailNewsScreen(
                                                  judul: newsControllerSlider.listnewsSlider[index].judul,
                                                  isi: newsControllerSlider.listnewsSlider[index].isi,
                                                  gambar: newsControllerSlider.listnewsSlider[index].gambar,
                                                  highlights: newsControllerSlider.listnewsSlider[index]
                                                      .highlights,
                                                  tgl_post: newsControllerSlider.listnewsSlider[index].tgl_post,
                                                )),
                                      );
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 14),
                                      child: Text(
                                        '${newsControllerSlider.listnewsSlider[index].judul}',
                                        style: TextStyle(
                                            fontSize: 17,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.02),
                                  InkWell(
                                    onTap: () {
                                      print('click highlight');
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailNewsScreen(
                                                judul: newsControllerSlider.listnewsSlider[index].judul,
                                                  isi: newsControllerSlider.listnewsSlider[index].isi,
                                                  gambar: newsControllerSlider.listnewsSlider[index].gambar,
                                                  highlights: newsControllerSlider.listnewsSlider[index]
                                                      .highlights,
                                                  tgl_post: newsControllerSlider.listnewsSlider[index].tgl_post,
                                                )),
                                      );
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 14),
                                      child: Text(
                                        '${newsControllerSlider.listnewsSlider[index].highlights}',
                                        style: TextStyle(
                                            color: Colors.black26,
                                            fontSize: 14),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.02,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                // Berita Slider
                : Center(
                    child: Column(
                      children:
                      [
                          SizedBox(height: 150,),
                          CircularProgressIndicator()
                      ],
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
