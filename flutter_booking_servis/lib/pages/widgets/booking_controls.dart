import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/controller/BookingController.dart';
import 'package:provider/provider.dart';

class BookingControls extends StatefulWidget {
  
  String id_customer;
  String username;

  BookingControls({this.username, this.id_customer});

  @override
  _BookingControlsState createState() => _BookingControlsState();
}

class _BookingControlsState extends State<BookingControls> {
  final TextEditingController _kodeBooking = new TextEditingController();
  final TextEditingController _noMesin = new TextEditingController();
  final TextEditingController _tglBooking = new TextEditingController();
  final TextEditingController _keluhan = new TextEditingController();
  final TextEditingController _tipeServis = new TextEditingController();

  final TextEditingController _idCustomer = new TextEditingController();

  String colorGroupValue = '';
  DateTime date = DateTime.now();
  String month = "";
  String day = "";
  String year = "";

  String inputKodeBooking;
  String inputNoMesin;
  String inputTglBooking;
  String inputKeluhan;
  String inputTipeServis;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime pickedDate = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != date)
      setState(() {
        date = pickedDate;
        day = date.toString().substring(8, 10);
        month = date.toString().substring(5, 7);
        year = date.toString().substring(0, 4);

        _tglBooking.text = "";
        _tglBooking.text = day + "-" + month + "-" + year;
        print(day);
      });
  }

  @override
  Widget build(BuildContext context) {
    final BookingController bookingController =
        Provider.of<BookingController>(context);
    bookingController.fetchKodeTrans();

    _idCustomer.text = widget.id_customer;

    _kodeBooking.text = (bookingController.listKodeTrans?.length != null)
        ? bookingController.listKodeTrans[0].kode_trans
        : '';
    return SafeArea(
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              // Kode Booking
              Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                  enabled: false,
                  controller: _kodeBooking,
                  decoration: const InputDecoration(labelText: 'Kode Trans'),
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    inputKodeBooking = value;
                  },
                ),
              ),
              // Kode booking

              // Nama Pemilik Kendaraan
              Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                  controller: _noMesin,
                  decoration: const InputDecoration(
                      labelText: 'No Mesin',
                      suffixIcon: Icon(Icons.keyboard_arrow_down_rounded)),
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    if (value.length > 0) {
                      // _tglBooking.text = "";
                      inputNoMesin = value;
                    }
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: GestureDetector(
                  onTap: () {
                    _selectDate(context);
                    day = date.toString().substring(8, 10);
                    month = date.toString().substring(5, 7);
                    year = date.toString().substring(0, 4);
                    _tglBooking.text = day + "-" + month + "-" + year;
                    inputTglBooking = _tglBooking.text;
                  },
                  child: AbsorbPointer(
                    child: TextField(
                      enabled: true,
                      controller: _tglBooking,
                      decoration: const InputDecoration(
                          labelText: 'Tanggal Booking',
                          suffixIcon: Icon(Icons.date_range)),
                      keyboardType: TextInputType.text,
                      onChanged: (value) {
                        inputTglBooking = value;
                      },
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: new Align(
                  alignment: Alignment.centerLeft,
                  child: new Text(
                    "Tipe Servis",
                    style: TextStyle(color: Color.fromRGBO(162, 155, 152, 0.7)),
                  ),
                ),
              ),
              Column(
                children: <Widget>[
                  Row(
                    children: [
                      Radio(
                        value: "Regular",
                        groupValue: colorGroupValue,
                        onChanged: (val) {
                          colorGroupValue = val;
                          _tipeServis.text = colorGroupValue;
                          print(colorGroupValue);
                          inputTipeServis = colorGroupValue;
                          setState(() {});
                        },
                      ),
                      Text('Servis Regular 3-4 Bulan')
                    ],
                  ),
                  Row(
                    children: [
                      Radio(
                        value: "Lainnya",
                        groupValue: colorGroupValue,
                        onChanged: (val) {
                          colorGroupValue = val;
                          _tipeServis.text = colorGroupValue;
                          print(colorGroupValue);
                          inputTipeServis = colorGroupValue;
                          setState(() {});
                        },
                      ),
                      Text('Servis Lainnya')
                    ],
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(top: 0, left: 10, right: 10),
                child: TextField(
                  enabled: (_tipeServis.text == "") ? false : true,
                  maxLines: 3,
                  controller: _keluhan,
                  decoration: const InputDecoration(labelText: 'Keluhan'),
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    inputKeluhan = value;
                  },
                ),
              ),

              Container(
                width: MediaQuery.of(context).size.width * 0.95,
                padding: EdgeInsets.only(top: 10),
                child: ElevatedButton(
                  child: Text('Booking Now'),
                  style: ElevatedButton.styleFrom(primary: Colors.indigo[300]),
                  // onPressed: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) => ConfirmationBooking(
                  //       kode_booking: _kodeBooking.text,
                  //       no_mesin: _noMesin.text,
                  //       tgl_booking: _tglBooking.text,
                  //       tipe_servis: _tipeServis.text,
                  //       keluhan: _keluhan.text,
                  //     ),
                  //   ),
                  // );
                  // },
                  onPressed: dispatchBooking,
                ),
              ),

              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future dispatchBooking() async {
  
  // print(_idCustomer.text);
  
    BookingController()
        .addBooking(_kodeBooking.text, _noMesin.text, _tglBooking.text,
            _tipeServis.text, _keluhan.text, _idCustomer.text)
        .then((value) {
      setState(() {
        if (value is String) {
          // print(value.toString());
          if(value.toString() == "ok")
          {
             ArtSweetAlert.show(context:context,
             artDialogArgs: ArtDialogArgs(
              title: "Sukses",
              text: "Berhasil Booking",
              type: ArtSweetAlertType.success)
             );
            _clearText();
          }
        }
      });
    });
    // 
    
   
  }

  _clearText()
  {
    _noMesin.text = "";
    _tglBooking.text = "";
    _tipeServis.text = "";
    _keluhan.text = "";
  }
}
