import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/pages/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'booking_screen.dart';
import 'history_screen.dart';
import 'home_screen.dart';
import 'profile_screen.dart';

class MasterMenu extends StatefulWidget {
  @override
  _MasterMenuState createState() => _MasterMenuState();
}

class _MasterMenuState extends State<MasterMenu> {
  int selectIndex = 0;

  String usernameParam;
  String id_customer;

  @override
  void initState() {
    super.initState();
    getSharePref();
  }

  getSharePref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      usernameParam = prefs.getString('username');
      id_customer = prefs.getString('id_customer');

      if (usernameParam == "" && id_customer == "") {
        Navigator.of(context).pop();
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => new LoginScreen()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Offstage(
            offstage: selectIndex != 0,
            child: TickerMode(
              enabled: selectIndex == 0,
              child: HomeScreen(),
            ),
          ),
          Offstage(
            offstage: selectIndex != 1,
            child: TickerMode(
              enabled: selectIndex == 1,
              child: BookingScreen(username:
                      (usernameParam?.length) != null ? usernameParam : '',
                  id_customer:
                      (id_customer?.length) != null ? id_customer : ''),
            ),
          ),
          Offstage(
            offstage: selectIndex != 2,
            child: TickerMode(
              enabled: selectIndex == 2,
              child: HistoryScreen(username:
                      (usernameParam?.length) != null ? usernameParam : '',
                  id_customer:
                      (id_customer?.length) != null ? id_customer : ''),
            ),
          ),
          Offstage(
            offstage: selectIndex != 3,
            child: TickerMode(
              enabled: selectIndex == 3,
              child: ProfileScreen(
                  username:
                      (usernameParam?.length) != null ? usernameParam : '',
                  id_customer:
                      (id_customer?.length) != null ? id_customer : ''),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Color.fromRGBO(255, 255, 255, 1),
        // color: Colors.blue,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              InkWell(
                onTap: () {
                  setState(() {
                    selectIndex = 0;
                    print(selectIndex);
                  });
                },
                child: Column(children: [
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.home,
                    color: (selectIndex == 0)
                        ? Color.fromRGBO(62, 122, 250, 1)
                        : Color.fromRGBO(185, 185, 185, 1),
                  ),
                  Text(
                    'Beranda',
                    style: TextStyle(
                        color: (selectIndex == 0)
                            ? Color.fromRGBO(62, 122, 250, 1)
                            : Color.fromRGBO(185, 185, 185, 1)),
                  )
                ]),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    selectIndex = 1;
                    print(selectIndex);
                  });
                },
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.car_repair,
                      color: (selectIndex == 1)
                          ? Color.fromRGBO(62, 122, 250, 1)
                          : Color.fromRGBO(185, 185, 185, 1),
                    ),
                    Text(
                      'Booking',
                      style: TextStyle(
                          color: (selectIndex == 1)
                              ? Color.fromRGBO(62, 122, 250, 1)
                              : Color.fromRGBO(185, 185, 185, 1)),
                    )
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    selectIndex = 2;
                    print(selectIndex);
                  });
                },
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.backpack,
                      color: (selectIndex == 2)
                          ? Color.fromRGBO(62, 122, 250, 1)
                          : Color.fromRGBO(185, 185, 185, 1),
                    ),
                    Text(
                      'Riwayat',
                      style: TextStyle(
                          color: (selectIndex == 2)
                              ? Color.fromRGBO(62, 122, 250, 1)
                              : Color.fromRGBO(185, 185, 185, 1)),
                    )
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    selectIndex = 3;
                    print(selectIndex);
                  });
                },
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.person_pin_sharp,
                      color: (selectIndex == 3)
                          ? Color.fromRGBO(62, 122, 250, 1)
                          : Color.fromRGBO(185, 185, 185, 1),
                    ),
                    Text(
                      'Akun',
                      style: TextStyle(
                          color: (selectIndex == 3)
                              ? Color.fromRGBO(62, 122, 250, 1)
                              : Color.fromRGBO(185, 185, 185, 1)),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
