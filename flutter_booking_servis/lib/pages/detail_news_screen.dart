import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/network/base_url.dart';

class DetailNewsScreen extends StatefulWidget {
  String judul;
  String highlights;
  String gambar;
  String isi;
  String tgl_post;

  DetailNewsScreen(
      {this.judul, this.highlights, this.gambar, this.isi, this.tgl_post});
      
  @override
  _DetailNewsScreenState createState() => _DetailNewsScreenState();
}

class _DetailNewsScreenState extends State<DetailNewsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Berita')),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(8),
          child: Column(
            children: [
              // Image.asset('assets/img/cbr.jpg'),
              Ink.image(
                height: MediaQuery.of(context).size.height * 0.25,
                // height:200,
                image: NetworkImage(Api.baseUrlImage + '${widget.gambar}'),
                fit: BoxFit.fitWidth,
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Diposting : ${widget.tgl_post}',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 14),
                  ),
                  Container(
                    width: 100,
                    height: 30,
                    margin: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      border: Border.all(color: Theme.of(context).primaryColor),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Center(
                      child: Text(
                        'Terbaru',
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      ),
                    ),
                  ),
                ],
              ),

              SizedBox(
                height: 15,
              ),
              // Judul dan isi
              Text(
                '${widget.judul}',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),

              // isi pake htmlwidget
              // Text('${widget.isi}'),
              // isi('${widget.isi}')
              // Judul dan isi
              //
               
              Text(widget.isi),
            ],
          ),
        ),
      ),
    );
  }
}
