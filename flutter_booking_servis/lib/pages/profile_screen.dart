import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/controller/ProfileController.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileScreen extends StatefulWidget {
  String id_customer;
  String username;

  ProfileScreen({this.username, this.id_customer});

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final TextEditingController _nama = new TextEditingController();
  final TextEditingController _alamat = new TextEditingController();
  final TextEditingController _noHp = new TextEditingController();
  final TextEditingController _email = new TextEditingController();
  final TextEditingController _username = new TextEditingController();
  final TextEditingController _password = new TextEditingController();

  String editNama;
  String editAlamat;
  String editNoHp;
  String editEmail;
  String editUsername;
  String editPassword;
  String idCustomer;

  @override
  Widget build(BuildContext context) {
    final ProfileController profileController =
        Provider.of<ProfileController>(context);
    profileController.fetchProfile(widget.id_customer);

    idCustomer = widget.id_customer;

    _nama.text = (profileController.listprofile?.length != null)
        ? profileController.listprofile[0].nama
        : '';

    _nama.selection =
        TextSelection.fromPosition(TextPosition(offset: _nama.text.length));

    _noHp.text = (profileController.listprofile?.length != null)
        ? profileController.listprofile[0].no_hp
        : '';
    _noHp.selection =
        TextSelection.fromPosition(TextPosition(offset: _noHp.text.length));

    _email.text = (profileController.listprofile?.length != null)
        ? profileController.listprofile[0].email
        : '';
    _email.selection =
        TextSelection.fromPosition(TextPosition(offset: _email.text.length));

    _username.text = (profileController.listprofile?.length != null)
        ? profileController.listprofile[0].username
        : '';
    _username.selection =
        TextSelection.fromPosition(TextPosition(offset: _username.text.length));

    _password.text = (profileController.listprofile?.length != null)
        ? profileController.listprofile[0].password
        : '';
    _password.selection =
        TextSelection.fromPosition(TextPosition(offset: _password.text.length));

    _alamat.text = (profileController.listprofile?.length != null)
        ? profileController.listprofile[0].alamat
        : '';
    _alamat.selection =
        TextSelection.fromPosition(TextPosition(offset: _alamat.text.length));

    return Scaffold(
        appBar: AppBar(
          title: Text('Profile'),
        ),
        body: profileController.listprofile?.length != null
            ? SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(8),
                  child: Container(
                    color: Colors.white,
                    child: Column(
                      children: [
                        Container(
                          // width: 315,
                          // height: MediaQuery.of(context).size.height * 0.1,
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: 20.0),
                                child: new Stack(
                                    fit: StackFit.loose,
                                    children: <Widget>[
                                      new Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Container(
                                              width: 140.0,
                                              height: 140.0,
                                              decoration: new BoxDecoration(
                                                shape: BoxShape.circle,
                                                image: new DecorationImage(
                                                  image: new ExactAssetImage(
                                                      'assets/img/as.jpeg'),
                                                  fit: BoxFit.cover,
                                                ),
                                              )),
                                        ],
                                      ),
                                    ]),
                              ),
                              // Image

                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12),
                                padding: EdgeInsets.all(10),
                                child: TextFormField(
                                  initialValue:
                                      profileController.listprofile[0].nama,
                                  enabled: true,
                                  // controller: _nama,
                                  decoration:
                                      const InputDecoration(labelText: 'Nama'),
                                  keyboardType: TextInputType.text,
                                  onChanged: (value) {
                                    editNama = value;
                                    // print(value);
                                  },
                                  onSaved: (value) {
                                    editNama = value;
                                    // print(value);
                                  },
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12),
                                padding: EdgeInsets.all(10),
                                child: TextFormField(
                                  initialValue:
                                      profileController.listprofile[0].no_hp,
                                  enabled: true,
                                  // controller: _noHp,
                                  decoration:
                                      const InputDecoration(labelText: 'No HP'),
                                  keyboardType: TextInputType.phone,
                                  onChanged: (value) {
                                    editNoHp = value;
                                    // print(value);
                                  },
                                  onSaved: (value) {
                                    editNoHp = value;
                                  },
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12),
                                padding: EdgeInsets.all(10),
                                child: TextFormField(
                                  initialValue:
                                      profileController.listprofile[0].email,
                                  enabled: true,
                                  // controller: _email,
                                  decoration:
                                      const InputDecoration(labelText: 'Email'),
                                  keyboardType: TextInputType.emailAddress,
                                  onChanged: (value) {
                                    editEmail = value;
                                    // print(value);
                                  },
                                  onSaved: (value) {
                                    editEmail = value;
                                  },
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12),
                                padding: EdgeInsets.all(10),
                                child: TextFormField(
                                  initialValue:
                                      profileController.listprofile[0].username,
                                  enabled: true,
                                  // controller: _username,
                                  decoration: const InputDecoration(
                                      labelText: 'Username'),
                                  keyboardType: TextInputType.text,
                                  onChanged: (value) {
                                    editUsername = value;
                                    // print(value);
                                  },
                                  onSaved: (value) {
                                    editUsername = value;
                                  },
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12),
                                padding: EdgeInsets.all(10),
                                child: TextFormField(
                                  initialValue:
                                      profileController.listprofile[0].password,
                                  enabled: true,
                                  // controller: _password,
                                  decoration: const InputDecoration(
                                      labelText: 'Password'),
                                  keyboardType: TextInputType.text,
                                  onChanged: (value) {
                                    editPassword = value;
                                    // print(value);
                                  },
                                  onSaved: (value) {
                                    editPassword = value;
                                  },
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12),
                                padding: EdgeInsets.all(10),
                                child: TextFormField(
                                  initialValue:
                                      profileController.listprofile[0].alamat,
                                  maxLines: 3,
                                  enabled: true,
                                  // controller: _alamat,
                                  decoration: const InputDecoration(
                                      labelText: 'Alamat'),
                                  keyboardType: TextInputType.text,
                                  onChanged: (value) {
                                    editAlamat = value;
                                    // print(value);
                                  },
                                  onSaved: (value) {
                                    editAlamat = value;
                                  },
                                ),
                              ),

                              Container(
                                width: MediaQuery.of(context).size.width * 0.8,
                                padding: EdgeInsets.only(top: 10),
                                child: ElevatedButton(
                                  child: Text('Update Profile'),
                                  style: ElevatedButton.styleFrom(
                                      primary: Colors.red[300]),
                                  onPressed: dispatchProfile,
                                ),
                              ),

                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.03,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            : Center(
                child: CircularProgressIndicator(),
              ));
  }

  Future dispatchProfile() async {
    String postNama,
        postNoHp,
        postEmail,
        postUsername,
        postPassword,
        postAlamat;

    (editNama == null) ? postNama = _nama.text : postNama = editNama.toString();
    (editNoHp == null) ? postNoHp = _noHp.text : postNoHp = editNoHp.toString();
    (editEmail == null)
        ? postEmail = _email.text
        : postEmail = editEmail.toString();

    (editUsername == null)
        ? postUsername = _username.text
        : postUsername = editUsername.toString();
    (editPassword == null)
        ? postPassword = _password.text
        : postPassword = editPassword.toString();
    (editAlamat == null)
        ? postAlamat = _alamat.text
        : postAlamat = editAlamat.toString();

    // print(postNama.toString()+""+postNoHp.toString()+""+postEmail+""+postUsername+""+postPassword+""+postAlamat);
    // print(idCustomer);
    //

    if (postNama.isEmpty ||
        postNoHp.isEmpty ||
        postEmail.isEmpty ||
        postUsername.isEmpty ||
        postPassword.isEmpty ||
        postAlamat.isEmpty) {
      ArtSweetAlert.show(
          context: context,
          artDialogArgs: ArtDialogArgs(
              title: "Perhatian...",
              text: "Inputan Tidak Boleh Kosong",
              type: ArtSweetAlertType.danger));
    } else {
      if (postNoHp.length > 12) {
        ArtSweetAlert.show(
            context: context,
            artDialogArgs: ArtDialogArgs(
                title: "Perhatian...",
                text: "No Hp Maksimal 12 Digit",
                type: ArtSweetAlertType.danger));
      } else {
        bool emailValid = EmailValidator.validate(postEmail.toString());

        if (!emailValid) {
          ArtSweetAlert.show(
              context: context,
              artDialogArgs: ArtDialogArgs(
                  title: "Perhatian...",
                  text: "Format Email Tidak Sesuai",
                  type: ArtSweetAlertType.danger));
        } else {
          // proses update
          ProfileController()
              .updateProfile(
                  postNama.toString(),
                  postNoHp.toString(),
                  postEmail.toString(),
                  postUsername.toString(),
                  postPassword.toString(),
                  postAlamat.toString(),
                  idCustomer.toString())
              .then((value) {
            setState(() {
              if (value is String) {
                //  print(value.toString());
                ArtSweetAlert.show(
                    context: context,
                    artDialogArgs: ArtDialogArgs(
                        title: "Sukses",
                        text: "Profil telah diperbaharui",
                        type: ArtSweetAlertType.success));
              }
            });
          });
          // proses update
        }
      }
    }
  }
}
