import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/controller/HistoryController.dart';
import 'package:flutter_booking_servis/pages/detail_history_screen.dart';
import 'package:provider/provider.dart';

class HistoryScreen extends StatefulWidget {

  String id_customer;
  String username;

  HistoryScreen({this.username, this.id_customer});

  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  @override
  Widget build(BuildContext context) {
    final HistoryController historyController =
        Provider.of<HistoryController>(context,listen: true);
    historyController.fetchHistory(widget.id_customer);
    // print(bookingController.listbooking[0].tgl_booking);

    return Scaffold(
        appBar: AppBar(title: Text('History')),
        body:
            // ListView.builder(
            //   itemCount: bookingController.listbooking.length,
            //   itemBuilder: (BuildContext context, int index){
            //     return Column(
            //       children: [
            //         Text(bookingController.listbooking[index].no_mesin),
            //       ],
            //     );
            //   }
            // ),

            historyController.listhistory?.length != null
                ? Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: SingleChildScrollView(
                      child: Column(
                              children: [
                                SizedBox(
                                  // height: 300,
                                  height: MediaQuery.of(context).size.height,
                                  child: ListView.builder(
                                    physics: ClampingScrollPhysics(),
                                    shrinkWrap: true,
                                    scrollDirection: Axis.vertical,
                                    itemCount:
                                        historyController.listhistory.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                          return  Container(
                                      // width: 315,
                                      // height: MediaQuery.of(context).size.height * 0.1,
                                      width: MediaQuery.of(context).size.width *
                                          0.92,
                                      child: InkWell(
                                        onTap: () {
                                          // print('click 2');
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailHistoryScreen(
                                                      tgl_servis:
                                                          historyController
                                                              .listhistory[index]
                                                              .tgl_servis,
                                                      kode_trans:
                                                          historyController
                                                              .listhistory[index]
                                                              .kode_trans,
                                                      no_pol: historyController
                                                          .listhistory[index]
                                                          .no_pol,
                                                      tipe_transaksi:
                                                          historyController
                                                              .listhistory[index]
                                                              .tipe_transaksi,
                                                      tipe_servis:
                                                          historyController
                                                              .listhistory[index]
                                                              .tipe_servis,
                                                      keluhan: historyController
                                                          .listhistory[index]
                                                          .keluhan,
                                                    )),
                                          );
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 5),
                                          child: Card(
                                            clipBehavior: Clip.antiAlias,
                                            elevation: 4,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(2)),
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    // print('click 1');
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              DetailHistoryScreen(
                                                                tgl_servis:
                                                                    historyController
                                                                        .listhistory[
                                                                            index]
                                                                        .tgl_servis,
                                                                kode_trans:
                                                                    historyController
                                                                        .listhistory[
                                                                            index]
                                                                        .kode_trans,
                                                                no_pol:
                                                                    historyController
                                                                        .listhistory[
                                                                            index]
                                                                        .no_pol,
                                                                tipe_transaksi:
                                                                    historyController
                                                                        .listhistory[
                                                                            index]
                                                                        .tipe_transaksi,
                                                                tipe_servis:
                                                                    historyController
                                                                        .listhistory[
                                                                            index]
                                                                        .tipe_servis,
                                                                keluhan:
                                                                    historyController
                                                                        .listhistory[
                                                                            index]
                                                                        .keluhan,
                                                              )),
                                                    );
                                                  },
                                                  child: Stack(
                                                    alignment:
                                                        Alignment.bottomLeft,
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets.only(
                                                                top: 15,
                                                                left: 10),
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                              child: Icon(
                                                                Icons
                                                                    .account_balance_wallet_outlined,
                                                                color: Colors
                                                                    .black38,
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 3,
                                                            ),
                                                            Text(
                                                              'Kami Menunggumu!',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black38,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 18),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.03,
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    // print('click judul');
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              DetailHistoryScreen(
                                                                tgl_servis: historyController
                                                  .listhistory[index].tgl_servis,
                                              kode_trans: historyController
                                                  .listhistory[index].kode_trans,
                                              no_pol: historyController
                                                  .listhistory[index].no_pol,
                                              tipe_transaksi: historyController
                                                  .listhistory[index].tipe_transaksi,
                                              tipe_servis: historyController
                                                  .listhistory[index].tipe_servis,
                                              keluhan: historyController
                                                  .listhistory[index].keluhan,
                                                              )),
                                                    );
                                                  },
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 14),
                                                    child: Text(
                                                      'Anda Memiliki Jadwal Servis pada tanggal ${historyController.listhistory[index].tgl_servis}',
                                                      style: TextStyle(
                                                        fontSize: 15,
                                                        color: Colors.black,
                                                      ),
                                                      maxLines: 2,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                    height: MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.015),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.symmetric(
                                                          horizontal: 14),
                                                  child: Text(
                                                    'Status Servis : ${historyController.listhistory[index].status_service}',
                                                    style: TextStyle(
                                                        color: Colors.redAccent,
                                                        fontSize: 14),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 2,
                                                  ),
                                                ),
                                                SizedBox(
                                                    height: MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.015),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.symmetric(
                                                          horizontal: 14),
                                                  child: Text(
                                                    'Diposting tanggal : ${historyController.listhistory[index].tgl_servis}',
                                                    style: TextStyle(
                                                        color: Colors.black26,
                                                        fontSize: 14),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 2,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.02,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                        }
                                  ),
                                ),
                              ],
                            ),
                    ),
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ));
  }
}
