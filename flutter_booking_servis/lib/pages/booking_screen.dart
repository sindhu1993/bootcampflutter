import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/pages/widgets/booking_controls.dart';

class BookingScreen extends StatefulWidget {
  
  String id_customer;
  String username;

  BookingScreen({this.username, this.id_customer});

  @override
  _BookingScreenState createState() => _BookingScreenState();
}

class _BookingScreenState extends State<BookingScreen> {
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Booking'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(5.0),
        child: BookingControls(username:
                      (widget.username?.length) != null ? widget.username : '',
                  id_customer:
                      (widget.id_customer?.length) != null ? widget.id_customer : ''),
      )
    );
  }
}
