import 'dart:async';
import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/network/base_url.dart';
import 'package:flutter_booking_servis/pages/master_menu.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();

  String usernameParam;
  String idCustomerParam;

  String _ready = "";
  Timer _timer;

  _checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      usernameParam = prefs.getString('username');
      idCustomerParam = prefs.getString('id_customer');

      if (idCustomerParam != null) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => new MasterMenu()));
      }
    });
  }

  addSharePref(String id_customer, String username) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('id_customer', id_customer);
      prefs.setString('username', username);
    });
  }

  _login() async {
    if (username.text.isEmpty || password.text.isEmpty) {
      ArtSweetAlert.show(
          context: context,
          artDialogArgs: ArtDialogArgs(
              title: 'Perhatian..',
              text: 'Username dan Password Harus Diisi',
              type: ArtSweetAlertType.danger));
    } else {
      final response = await http.post(Uri.parse(Api.baseUrl + "login"),
          body: {"username": username.text, "password": password.text});
      // print(response.body);
      var dataUser = json.decode(response.body);

      if (dataUser.length == 0) {
        setState(() {
          // msg = "Login Fail";
          ArtSweetAlert.show(
              context: context,
              artDialogArgs: ArtDialogArgs(
                  title: 'Login Gagal',
                  text: 'Username atau Password Salah',
                  type: ArtSweetAlertType.danger));
        });
      } else {
        setState(() {
          usernameParam = dataUser[0]['username'];
          idCustomerParam = dataUser[0]['id_customer'];
          addSharePref(idCustomerParam, usernameParam);
        });

        Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => new MasterMenu()));

        // ArtSweetAlert.show(
        //     context: context,
        //     artDialogArgs: ArtDialogArgs(
        //         title: "Sukses Login",
        //         text: "Klik Ok untuk melanjutkan",
        //         type: ArtSweetAlertType.success,
        //         showCancelBtn: false,
        //         onConfirm: (bool isConfirm) {
        //           if (isConfirm) {
        //             // ArtSweetAlert.show(context,style: ArtSweetAlertStyle.success,title: "Success");
        //             Navigator.of(context).pop();
        //             Navigator.pushReplacement(
        //                 context,
        //                 MaterialPageRoute(
        //                     builder: (context) => new MasterMenu()));
        //             // return false to keep dialog
        //             return false;
        //           }
        //         }));
      }
    }
  }

  bool _secureText = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  @override
  void initState() {
    super.initState();
    _checkLogin();
  }

  Future<String> _calculation = Future<String>.delayed(
    Duration(seconds: 2),
    () => 'Mekanik Kami Sudah Siap',
  );

  Widget _loadingWidget(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 1,
      child: Center(
        child: Column(
          children: [
            // SizedBox(height: MediaQuery.of(context).size.height * 0.48),
            FutureBuilder(
                future: _calculation,
                builder:
                    (BuildContext context, AsyncSnapshot<String> snapshot) {
                  List<Widget> children;
                  if (snapshot.hasData) {
                    children = <Widget>[_loginWidget(context)];
                  } else if (snapshot.hasError) {
                    children = <Widget>[
                      Icon(
                        Icons.error_outline,
                        color: Colors.red,
                        size: 60,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text('Error: ${snapshot.error}'),
                      )
                    ];
                  } else {
                    children = <Widget>[
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.45),
                      SizedBox(
                        child: CircularProgressIndicator(),
                        width: 30,
                        height: 30,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: Column(
                          children: [
                            Text('Sabar...'),
                            Text('Mekanik Kami Lagi Siap - Siap...'),
                          ],
                        ),
                      )
                    ];
                  }
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: children,
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }

  Widget _loginWidget(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 40.0),
          child: Center(
            child: Container(
                width: MediaQuery.of(context).size.width * 0.5,
                height: MediaQuery.of(context).size.height * 0.4,
                child: Image.asset(
                  'assets/img/logo.png',
                )),
          ),
        ),
        Padding(
          //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: TextFormField(
            controller: username,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Username',
                hintText: 'Enter valid username'),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
              left: 15.0, right: 15.0, top: 15, bottom: 0),
          //padding: EdgeInsets.symmetric(horizontal: 15),
          child: TextFormField(
            controller: password,
            obscureText: _secureText,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
                hintText: 'Enter secure password',
                suffixIcon: IconButton(
                    icon: Icon(
                        _secureText ? Icons.visibility : Icons.visibility_off),
                    onPressed: showHide)),
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.035,
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.1,
          width: MediaQuery.of(context).size.width * 0.9,
          decoration: BoxDecoration(
              color: Colors.blue, borderRadius: BorderRadius.circular(30)),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
            ),
            onPressed: () {
              _login();
            },
            child: Text(
              'Login',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        // child: _loginWidget(context),
        child: Column(
          children: [
            _loadingWidget(context),
            // _loginWidget(context)
          ],
        ),
      ),
    );
  }
}
