import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/controller/ProfileController.dart';
import 'package:flutter_booking_servis/pages/login_screen.dart';
import 'package:flutter_booking_servis/pages/splash_screen.dart';
import 'controller/BannerController.dart';
import 'controller/BookingController.dart';
import 'controller/NewsController.dart';
import 'package:provider/provider.dart';
import 'controller/HistoryController.dart';
import 'pages/master_menu.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => BookingController(),
        ),
        ChangeNotifierProvider(
          create: (_) => NewsController()
        ),
        ChangeNotifierProvider(
          create: (_) => BannerController()
        ),
        ChangeNotifierProvider(
          create: (_) => HistoryController()
        ),
         ChangeNotifierProvider(
          create: (_) => ProfileController()
        ),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      
      // home: MasterMenu(),
      // home: LoginScreen(),
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}