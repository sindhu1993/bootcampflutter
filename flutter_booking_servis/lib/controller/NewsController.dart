import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_booking_servis/model/NewsModel.dart';
import 'package:flutter_booking_servis/network/base_url.dart';
import 'package:http/http.dart' as http;

class NewsController extends ChangeNotifier {
  List<NewsModel> _news;
  List<NewsModel> get listnews => _news;

  List<NewsModel> _newsSlider;
  List<NewsModel> get listnewsSlider => _newsSlider;

  set listnews(List<NewsModel> val){
    _news = val;
    notifyListeners();
  }

  set listnewsSlider(List<NewsModel> val){
    _newsSlider = val;
    notifyListeners();
  }
  
  // Fetch All News
  Future<List<NewsModel>> fetchNews() async{
    final response = await http.get(Uri.parse(Api.baseUrl+"list_news"));

    List res = jsonDecode(response.body);
    List<NewsModel> data = [];

    for(var i=0; i<res.length;i++){
      var booking = NewsModel.fromJson(res[i]);
      data.add(booking);
    }
    listnews = data;
    return listnews;
  }
  // Fetch All News
  
  // Fetch Slider
  Future<List<NewsModel>> fetchNewsSlider() async{
    final response = await http.get(Uri.parse(Api.baseUrl+"list_news_slider"));

    List res = jsonDecode(response.body);
    List<NewsModel> data = [];

    for(var i=0; i<res.length;i++){
      var booking = NewsModel.fromJson(res[i]);
      data.add(booking);
    }
    listnewsSlider = data;
    return listnewsSlider;
  }
  // Fetch Slider
}
