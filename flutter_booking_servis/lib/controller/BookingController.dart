import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_booking_servis/model/BookingModel.dart';
import 'package:flutter_booking_servis/network/base_url.dart';
import 'package:http/http.dart' as http;

class BookingController extends ChangeNotifier {
  List<BookingModel> _booking;
  List<BookingModel> get listbooking => _booking;

  List<BookingModel> _kodeTrans;
  List<BookingModel> get listKodeTrans => _kodeTrans;

  set listbooking(List<BookingModel> val){
    _booking = val;
    notifyListeners();
  }

  set listKodeTrans(List<BookingModel> val){
    _kodeTrans = val;
    notifyListeners();
  }

  // Add Booking
  Future<String> addBooking(String kode_trans, String no_mesin,
      String tgl_booking, String tipe_servis, String keluhan, String id_customer) async {

    final response = await http.post(
      Uri.parse(Api.baseUrl + "insert_booking"),
      headers: {"Content-type": "application/x-www-form-urlencoded"},
      body: {
        'kode_trans': kode_trans,
        'no_mesin': no_mesin,
        'tgl_booking': tgl_booking,
        'tipe_servis': tipe_servis,
        'keluhan': keluhan,
        'id_customer':id_customer
      },
    );

    if(response.statusCode == 200){
      // print('sukses nambah data');
      // return response.statusCode;
      return "ok";
    }else{
      throw Exception('gagal booking');
    }
  }
  // Add Booking
  
  // Fetch List Order Booking
  Future<List<BookingModel>> fetchKodeTrans() async{
    final response = await http.get(Uri.parse(Api.baseUrl+"get_kode_trans"));

    List res = jsonDecode(response.body);
    List<BookingModel> data = [];

    for(var i=0; i<res.length;i++){
      var kode_trans = BookingModel.fromJson(res[i]);
      data.add(kode_trans);
    }
    listKodeTrans = data;
    return listKodeTrans;
  }
  // Fetch List Order Booking
}
