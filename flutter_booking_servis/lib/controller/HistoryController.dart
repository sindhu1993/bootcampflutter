import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/model/HistoryModel.dart';
import 'package:flutter_booking_servis/network/base_url.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class HistoryController extends ChangeNotifier{
  List<HistoryModel> _history;
  List<HistoryModel> get listhistory => _history;

  set listhistory(List<HistoryModel> val){
    _history = val;
    notifyListeners();
  }
  
  // Fetch List History
  Future<List<HistoryModel>> fetchHistory(String id_customer) async{
    final response = await http.post(Uri.parse(Api.baseUrl+"list_history",),
     headers: {"Content-type": "application/x-www-form-urlencoded"},
      body: {
        'id_customer':id_customer
      });

    List res = jsonDecode(response.body);
    List<HistoryModel> data = [];

    for(var i=0; i<res.length;i++){
      var history = HistoryModel.fromJson(res[i]);
      data.add(history);
    }
    listhistory = data;
    return listhistory;
  }
  // Fetch List History
}