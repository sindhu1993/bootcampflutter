class HistoryModel {
  final String kode_trans;
  final String est_lama_service;
  final String status_service;
  final String tipe_transaksi;
  final String keluhan;
  final String no_pol;
  final String tgl_servis;
  final String tipe_servis;

  HistoryModel({this.kode_trans, this.est_lama_service, this.status_service, this.tipe_transaksi,
  this.keluhan, this.no_pol, this.tgl_servis, this.tipe_servis});

  factory HistoryModel.fromJson(Map<String, dynamic> json){
    return HistoryModel(
      kode_trans: json['kode_trans'],
      est_lama_service: json['est_lama_service'],
      status_service: json['status_service'],
      tipe_transaksi: json['tipe_transaksi'],
      keluhan: json['keluhan'],
      no_pol: json['no_pol'],
      tgl_servis: json['tgl_servis'],
      tipe_servis: json['tipe_servis']
    );
  }
}