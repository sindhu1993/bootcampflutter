
class BookingModel {
  final String kode_trans;
  final String no_mesin;
  final String tgl_booking;
  final String tipe_servis;
  final String keluhan;

  BookingModel({this.kode_trans, this.no_mesin, this.tgl_booking, this.tipe_servis,
  this.keluhan});

  factory BookingModel.fromJson(Map<String, dynamic> json){
    return BookingModel(
      kode_trans: json['kode_trans'],
      no_mesin: json['no_mesin'],
      tgl_booking: json['tgl_booking'],
      tipe_servis: json['tipe_servis'],
      keluhan: json['keluhan']
    );
  }
}