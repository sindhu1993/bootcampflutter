

import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  AttackTitan attackTitan = AttackTitan();
  ArmorTitan  armorTitan = ArmorTitan();
  BeastTitan beastTitan = BeastTitan();
  Human human = Human();

  attackTitan.powerPoint = 1;
  armorTitan.powerPoint = 5;
  beastTitan.powerPoint = 10;
  human.powerPoint = 15;

  print("level point attack titan : ${attackTitan.powerPoint}");
  print("level point armor titan : ${armorTitan.powerPoint}");
  print("level point beast titan : ${beastTitan.powerPoint}");
  print("level point human : ${human.powerPoint}");

  print("sifat dari attack titan : "+attackTitan.punch());
  print("level point armor titan : "+armorTitan.terjang());
  print("level point beast titan : "+beastTitan.lempar());
  print("level point human : "+human.killAlltitan());
}