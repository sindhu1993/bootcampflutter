

import 'lingkaran.dart';
import 'bangun_datar.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  OperasiHitung operasiHitung = new OperasiHitung();
  Segitiga segitiga = new Segitiga(12, 6);
  Persegi persegi = new Persegi(4);
  Lingkaran lingkaran = new Lingkaran(14);

  operasiHitung.luas();
  operasiHitung.keliling();

  print("Luas Segitiga : ${segitiga.luas()}");
  print("Keliling Segitiga : ${segitiga.keliling()}");

  print("Luas Persegi : ${persegi.luas()}");
  print("Keliling Persegi : ${persegi.keliling()}");

  print("Luas Lingkaran : ${lingkaran.luas()}");
  print("Keliling Lingkaran : ${lingkaran.keliling()}");
}