import 'bangun_datar.dart';

class Persegi extends OperasiHitung{

  // luas sisi * sisi dan keliling =  2 * sisi

  double sisi = 0.0;

  Persegi(double sisi){
    this.sisi = sisi;
  }

  @override
  double luas(){
    return sisi * sisi;
  }

  @override
  double keliling(){
    return 2 * sisi;
  }
}