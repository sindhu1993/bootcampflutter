import 'bangun_datar.dart';

class Lingkaran extends OperasiHitung{

  // luas = 3.14 * jari-jari * jari-jari, keliling = 2 * pi * jari-jari 

  double jarijari = 0.0;

  Lingkaran(double jarijari){
    this.jarijari = jarijari;
  }

  @override
  double luas(){
    double phi = 3.14;
    return phi * jarijari * jarijari;
  }

  @override
  double keliling(){
    double phi = 3.14;
    return 2 * phi * jarijari;
  }
}