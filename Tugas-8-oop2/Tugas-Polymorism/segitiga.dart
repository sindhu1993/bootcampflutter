import 'dart:math';

import 'bangun_datar.dart';

class Segitiga extends OperasiHitung{

  // luas = 0.5 * alas * tinggi, keliling = a +b + t

  double alas = 0.0;
  double tinggi = 0.0;
  double sisimiring = 0.0;

  Segitiga(double alas, double tinggi){
    this.alas = alas;
    this.tinggi = tinggi;
  }

  @override
  double luas(){
    return 0.5 * alas * tinggi;
  }

  @override
  double keliling(){
    sisimiring = sqrt((alas * alas) + (tinggi*tinggi));
    return sisimiring + alas + tinggi;
  }
}