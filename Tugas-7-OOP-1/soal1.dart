

import 'lingkaran.dart';

void main(List<String> args) {

  // no 1
  Segitiga segitiga;
  double luasSegitiga;

  segitiga = new Segitiga();
  segitiga.setengah = 0.5;
  segitiga.alas = 20.0;
  segitiga.tinggi = 30.0;

  luasSegitiga = segitiga.hitungluasSegitiga();
  print(luasSegitiga);
  // no 1

  // no 2
  Lingkaran lingkaran;
  double luasLingkaran;
  lingkaran = new Lingkaran();
  lingkaran.jarijari = 2.0;
  
  luasLingkaran = lingkaran.luas;
  print(luasLingkaran);
  // no 2
}

// no 1
class Segitiga{
  double setengah = 0.0;
  double alas = 0.0;
  double tinggi = 0.0;
  
  double hitungluasSegitiga(){
    return this.setengah * this.alas * this.tinggi;
  }
}
// no 1