
class Lingkaran{
  double phi = 3.14;
  double _jarijari = 0.0;

  void set jarijari(double value){
    if(value < 0){
      value *= -1;
    }

    _jarijari=value;
  }

  double get jarijari{
    return _jarijari;
  }

  double get luas => phi * (_jarijari * _jarijari);
}