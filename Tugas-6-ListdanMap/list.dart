
void main(List<String> args) {
  range(1,10);
  range(10, 1);

  rangeWithStep(1, 10, 2);
  rangeWithStep(10, 1, 2);

  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ];
  dataHandling(input);

  balikKata("Kasur");
}

// No 1
range(startNum, finishNum){
  if(startNum < finishNum){
    for(int x=startNum; x<=finishNum; x++){
      print(x);
    }
  }else{
    if(startNum > finishNum){
      var arrData = [];
      
      for(int x=finishNum; x<=startNum; x++){
        arrData.add(x);
      }

      arrData.sort((a, b)=> b.compareTo(a));
      arrData.forEach((element) {
          print(element);
      });
    }
  }
}
// No 1

// No 2
rangeWithStep(startNum, finishNum, num step) {
  if(startNum < finishNum){
    for(num x=startNum; x<=finishNum; x+= step){
      print(x);
    }
  }else{
    if(startNum > finishNum){
      var arrData = [];
      
      for(num x=finishNum; x<=startNum; x+= step){
        arrData.add(x);
      }

      arrData.sort((a, b)=> b.compareTo(a));
      arrData.forEach((element) {
          print(element);
      });
    }
  }
}
// No 2

// No 3
dataHandling(input){
  var stringResult = "";
  for(int x=0; x<input.length; x++){
    stringResult += "\nNomor ID : ${input[x][0]} \nNama Lengkap : ${input[x][1]} \nTTL : ${input[x][2]} \nHobi : ${input[x][3]}\n";
  }

  print(stringResult);
}
// No 3

// No 4
balikKata(words){
  var currentWords = words;
  var newWords = '';

  for(num i = words.length - 1; i >= 0; i--)
  {
      newWords = newWords + currentWords[i];
  }
  print(newWords);
}
// No 4