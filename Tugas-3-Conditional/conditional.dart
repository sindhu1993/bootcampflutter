import 'dart:io';

void main(List<String> args) {

  // No 1
  print("Apakah anda mau install aplikasi ini ? : ");
  String inputText = stdin.readLineSync()!;
  if(inputText == "Y" || inputText == "y")
  {
    print('anda akan menginstall aplikasi dart');
  }
  else
  {
    if(inputText == "T" || inputText == "t")
    {
      print('Aborted');
    }
    else{
      print('Masukkan jawaban dengan pilihan Y/y atau T/t');
    }
  }
  // No 1

  // No 2
  String hasilNama = "";
  String hasilPeran = "";
  print('masukkan nama : ');
  String nama = stdin.readLineSync()!;
  hasilNama = nama;
  print('masukkan peran : ');
  String peran = stdin.readLineSync()!;
  hasilPeran = peran;

  if(hasilNama == "" && hasilPeran == "")
  {
    print('Nama harus diisi');
  }
  else
  {
    if(hasilNama != "" && hasilPeran == "")
    {
      print('Halo ${hasilNama}, Pilih peranmu untuk memulai game!');
    }

    else
    {
      if(hasilNama != "" && hasilPeran == "Penyihir")
      {
        print('Selamat datang di Dunia Werewolf, ${hasilNama}');
        print('Halo ${hasilPeran} ${hasilNama}, kamu dapat melihat siapa yang menjadi werewolf!');
      }
      else
      {
        if(hasilNama != "" && hasilPeran == "Guard")
        {
          print('Selamat datang di Dunia Werewolf, ${hasilNama}');
          print('Halo ${hasilPeran} ${hasilNama}, kamu akan membantu melindungi temanmu dari serangan werewolf.');
        }
        else
        {
          if(hasilNama != "" && hasilPeran == "Werewolf")
          {
            print('Selamat datang di Dunia Werewolf, ${hasilNama}');
            print('Halo ${hasilPeran} ${hasilNama}, Kamu akan memakan mangsa setiap malam!');
          }
        }
      }
    }
  }
  // No 2

  // No 3

  print("Masukkan nama hari : ");
  String day = stdin.readLineSync()!;
  switch (day) {
    case "Senin": {print('Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');break;}
    case "Selasa" : {print('Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');break;} 
    case "Rabu" : {print('Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');break;}
    case "Kamis" : {print('Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');break;}
    case "Jumat" : {print('Hidup tak selamanya tentang pacar.');break;}
    case "Sabtu" : {print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');break;}
    case "Minggu" : {print('Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');break;}
    default:print('Tidak ada hari yg ada');
  }

  // No 3

  // No 4
  var tanggal = 1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31) 
  var bulan = 0; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12) 
  var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

  String namabulan = "";
  switch (bulan) {
    case 1: {print(namabulan = 'Januari');break;}
    case 2 : {print(namabulan = 'Februari');break;} 
    case 3 : {print(namabulan = 'Maret');break;}
    case 4 : {print(namabulan = 'April');break;}
    case 5 : {print(namabulan = 'Mei');break;}
    case 6 : {print(namabulan = 'Juni');break;}
    case 7 : {print(namabulan = 'Juli');break;}
    case 8: {print(namabulan = 'Agustus');break;}
    case 9 : {print(namabulan = 'September');break;} 
    case 10 : {print(namabulan = 'Oktober');break;}
    case 11: {print(namabulan = 'November');break;}
    case 12 : {print(namabulan = 'Desember');break;}
    default:print('Tidak ada nama bulan');break;
  }

  if(tanggal < 1 && tanggal > 31)
  {
    print('masukkan tanggal dari angka 1 - 31');
  }
  else
  {
    if(tahun < 1900 && tahun > 2200)
    {
       print('masukkan tahun dari angka 1900 - 2200');
    }

    else
    {
      print('Hasil : ${tanggal} ${namabulan} $tahun');
    }
  }
  // No 4  
} 