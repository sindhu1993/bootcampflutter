

void main(List<String> args) {
  teriak();
  kalikan(12,4);
  introduce("Agus",40,"Jl Malioboro","Gaming");
  faktorial(4);
}

// No 1
teriak(){
  print('Halo Sanbers!');
}
// No 1

// No 2
var num1 = 0;
var num2 = 0;

kalikan(num1, num2)
{
  if(num1 == "" || num2 == "")
  {
    print('Masukkan angka pertama dan kedua');
  }

  else
  {
    var hasilKali = num1 * num2;
    print(hasilKali);
  }
}
// No 2

// No 3
RegExp _numeric = RegExp(r'^-?[0-9]+$');
introduce(name, age, address, hobby)
{
  if(name == "" || age == "" || address == "" || hobby == "")
  {
    print('Parameter nama, umur, alamat dan hobby harus dimasukkan');
  }
  else
  {
    if(_numeric.hasMatch(age.toString()))
    {
      var hasil = "Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!";
      print(hasil);
    }

    else
    {
      print('Umur harus angka 1-9');
    }
  }
}
// No 3

// No 4
faktorial(angka)
{
  RegExp _numeric = RegExp(r'^-?[0-9]+$');
  if(_numeric.hasMatch(angka.toString()))
  {
    int hasilAngka = 1;
    for(int i = angka; i >0; i--)
    {
      hasilAngka *= i;
    }
    print(hasilAngka);
  }

  else
  {
    print('Masukkan angka 1-9');
  }
}
// No 4

