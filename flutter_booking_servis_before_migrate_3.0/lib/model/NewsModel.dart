class NewsModel {
  final String judul;
  final String highlights;
  final String isi;
  final String gambar;
  final String tgl_post;

  NewsModel({this.judul, this.highlights, this.isi, this.gambar,
  this.tgl_post});

  factory NewsModel.fromJson(Map<String, dynamic> json){
    return NewsModel(
      judul: json['judul'],
      highlights: json['highlights'],
      isi: json['isi'],
      gambar: json['gambar'],
      tgl_post: json['tgl_post']
    );
  }
}