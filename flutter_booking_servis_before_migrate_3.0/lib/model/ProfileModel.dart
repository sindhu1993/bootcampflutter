class ProfileModel {
  final String nama;
  final String alamat;
  final String no_hp;
  final String email;
  final String username;
  final String password;

  ProfileModel({this.nama, this.alamat, this.no_hp, this.email,
  this.username, this.password});

  factory ProfileModel.fromJson(Map<String, dynamic> json){
    return ProfileModel(
      nama: json['nama'],
      alamat: json['alamat'],
      no_hp: json['no_hp'],
      email: json['email'],
      username: json['username'],
      password: json['password']
    );
  }
}