class BannerModel {
  final String judul;
  final String isi;
  final String gambar;
  final String tgl_post;

  BannerModel({this.judul,  this.isi, this.gambar,
  this.tgl_post});

  factory BannerModel.fromJson(Map<String, dynamic> json){
    return BannerModel(
      judul: json['judul'],
      isi: json['isi'],
      gambar: json['gambar'],
      tgl_post: json['tgl_post']
    );
  }
}