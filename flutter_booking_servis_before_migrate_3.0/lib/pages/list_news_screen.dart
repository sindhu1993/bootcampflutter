import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/controller/NewsController.dart';
import 'package:flutter_booking_servis/network/base_url.dart';
import 'package:provider/provider.dart';

import 'detail_news_screen.dart';

class ListNewsScreen extends StatefulWidget {
  @override
  _ListNewsScreenState createState() => _ListNewsScreenState();
}

class _ListNewsScreenState extends State<ListNewsScreen> {
  @override
  Widget build(BuildContext context) {
    final NewsController newsController = Provider.of<NewsController>(context);
    newsController.fetchNews();

    return Scaffold(
      appBar: AppBar(
        title: Text('Daftar Berita'),
      ),
      body:
          // Text('${newsController.listnews?.length}')
          newsController.listnews?.length != null
              ? SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Container(
                      color: Colors.white,
                      child: Column(
                        children: [
                          SizedBox(
                            // height: 300,
                            height: MediaQuery.of(context).size.height,
                            child: ListView.builder(
                              padding: EdgeInsets.only(bottom: 100),
                              physics: ClampingScrollPhysics(),
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              itemCount: newsController.listnews.length,
                              itemBuilder: (BuildContext context, int index) =>
                                  Container(
                                // width: 315,
                                // height: MediaQuery.of(context).size.height * 0.1,
                                width: MediaQuery.of(context).size.width * 0.92,
                                child: InkWell(
                                  onTap: () {
                                    print('click 2');
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => DetailNewsScreen(
                                          judul: newsController
                                              .listnews[index].judul,
                                          isi: newsController
                                              .listnews[index].isi,
                                          gambar: newsController
                                              .listnews[index].gambar,
                                          highlights: newsController
                                              .listnews[index].highlights,
                                          tgl_post: newsController
                                              .listnews[index].tgl_post,
                                        ),
                                      ),
                                    );
                                  },
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 5),
                                    child: Card(
                                      clipBehavior: Clip.antiAlias,
                                      elevation: 4,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(2)),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              // print('click 1');
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DetailNewsScreen(
                                                          judul: newsController
                                                              .listnews[index]
                                                              .judul,
                                                          isi: newsController
                                                              .listnews[index]
                                                              .isi,
                                                          gambar: newsController
                                                              .listnews[index]
                                                              .gambar,
                                                          highlights:
                                                              newsController
                                                                  .listnews[
                                                                      index]
                                                                  .highlights,
                                                          tgl_post:
                                                              newsController
                                                                  .listnews[
                                                                      index]
                                                                  .tgl_post,
                                                        )),
                                              );
                                            },
                                            child: Stack(
                                              alignment: Alignment.bottomLeft,
                                              children: [
                                                Ink.image(
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.25,
                                                  // height:200,
                                                  image: NetworkImage(Api
                                                          .baseUrlImage +
                                                      '${newsController.listnews[index].gambar}'),
                                                  fit: BoxFit.fitWidth,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.03,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              // print('click judul');
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DetailNewsScreen(
                                                          judul: newsController
                                                              .listnews[index]
                                                              .judul,
                                                          isi: newsController
                                                              .listnews[index]
                                                              .isi,
                                                          gambar: newsController
                                                              .listnews[index]
                                                              .gambar,
                                                          highlights:
                                                              newsController
                                                                  .listnews[
                                                                      index]
                                                                  .highlights,
                                                          tgl_post:
                                                              newsController
                                                                  .listnews[
                                                                      index]
                                                                  .tgl_post,
                                                        )),
                                              );
                                            },
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 14),
                                              child: Text(
                                                '${newsController.listnews[index].judul}',
                                                style: TextStyle(
                                                    fontSize: 17,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.02),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 14),
                                            child: Text(
                                              '${newsController.listnews[index].highlights}',
                                              style: TextStyle(
                                                  color: Colors.black26,
                                                  fontSize: 14),
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 2,
                                            ),
                                          ),
                                          SizedBox(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.02,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : Center(child: CircularProgressIndicator()),
    );
  }
}
