import 'package:flutter/material.dart';

class DetailHistoryScreen extends StatefulWidget {

  String tgl_servis;
  String kode_trans;
  String no_pol;
  String tipe_transaksi;
  String tipe_servis;
  String keluhan;

  DetailHistoryScreen(
      {this.tgl_servis, this.kode_trans, this.no_pol, 
      this.tipe_transaksi, this.tipe_servis, this.keluhan});

  @override
  _DetailHistoryScreenState createState() => _DetailHistoryScreenState();
}

class _DetailHistoryScreenState extends State<DetailHistoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Servis'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 15, left: 15, right: 15),
          child: Container(
            child: Column(
              children: [
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Detail Servis Anda',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 17),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  // height: 20,
                  height: MediaQuery.of(context).size.height * 0.03,
                ),
                Container(
                  child: Column(
                    children: [
                      Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              // kiri
                              Icon(
                                Icons.date_range_rounded,
                                color: Colors.black45,
                                size: 20,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(2),
                                child: Text(
                                  '${widget.tgl_servis}',
                                  style: TextStyle(fontSize: 14),
                                ),
                              ),
                              // kiri

                              // tengah
                              Padding(
                                padding: const EdgeInsets.only(left: 40),
                                child: Icon(
                                  Icons.timer,
                                  color: Colors.black45,
                                  size: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(2),
                                child: Text(
                                  '09:00',
                                  style: TextStyle(fontSize: 14),
                                ),
                              ),
                              // tengah

                              // kanan
                              Padding(
                                padding: const EdgeInsets.only(left: 50),
                                child: Icon(
                                  Icons.car_repair,
                                  color: Colors.black45,
                                  size: 20,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(2),
                                child: Text(
                                  '${widget.kode_trans}',
                                  style: TextStyle(fontSize: 14),
                                ),
                              ),
                              // kanan
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  // height: 25,
                  height: MediaQuery.of(context).size.height * 0.031,
                ),

                // no polisi
                Container(
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left:5),
                        child: Text(
                          'Nomor Polisi',
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  // height: 15,
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                Container(
                  child: SizedBox(
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left:5),
                          child: (widget.no_pol !=null) ?
                          Text(
                            '${widget.no_pol}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                          : Text(
                            '-',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  // height: 10,
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Divider(
                    color: Colors.indigo,
                    height: 10,
                  ),
                ),
                // no polisi

                // Tipe Booking
                Container(
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left:5,top:10),
                        child: Text(
                          'Tipe Transaksi',
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  // height: 15,
                  height: MediaQuery.of(context).size.height * 0.025,
                ),
                Container(
                  child: SizedBox(
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left:5,top:10),
                          child: Text(
                            '${widget.tipe_transaksi}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  // height: 10,
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Divider(
                    color: Colors.indigo,
                    height: 10,
                  ),
                ),
                // Tipe Booking
                
                // Tipe Servis
                Container(
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left:5,top:10),
                        child: Text(
                          'Tipe Servis',
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  // height: 15,
                  height: MediaQuery.of(context).size.height * 0.03,
                ),
                Container(
                  child: SizedBox(
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left:5),
                          child: Text(
                            '${widget.tipe_servis}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  // height: 10,
                  height: MediaQuery.of(context).size.height * 0.03,
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Divider(
                    color: Colors.indigo,
                    height: 10,
                  ),
                ),
                
                // Tipe Servis

                // Keluhan
                Container(
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left:5,top:10),
                        child: Text(
                          'Keluhan',
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  // height: 15,
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                Container(
                  child: SizedBox(
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left:5),
                          child: Text(
                            '${widget.keluhan}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                
                // Keluhan 
              ],
            ),
          ),
        ),
      ),
    );
  }
}
