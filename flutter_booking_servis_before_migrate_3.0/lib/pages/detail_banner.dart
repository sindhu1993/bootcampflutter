import 'package:flutter/material.dart';
import 'package:flutter_booking_servis/network/base_url.dart';

class DetailBannerScreen extends StatefulWidget {

  String judul;
  String gambar;
  String isi;
  String tgl_post;

  DetailBannerScreen(
      {this.judul, this.gambar, this.isi, this.tgl_post});

  @override
  _DetailBannerScreenState createState() => _DetailBannerScreenState();
}

class _DetailBannerScreenState extends State<DetailBannerScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Banner'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(8),
          child: Column(
            children: [
              Ink.image(
                height: MediaQuery.of(context).size.height * 0.25,
                // height:200,
                image: NetworkImage(Api.baseUrlImage + '${widget.gambar}'),
                fit: BoxFit.fitWidth,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.045,
              ),
              
              // ambil dari htmlwidget
              Text(
                '${widget.judul}',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.bold),
              ),

              SizedBox(
                height: 15,
              ),
              // Judul dan isi
             
              // isi pake htmlwidget
              Text('${widget.isi}',)
              // Judul dan isi
            ],
          ),
        ),
      ),
    );
  }
}
