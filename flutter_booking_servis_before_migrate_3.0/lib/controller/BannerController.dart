import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_booking_servis/model/BannerModel.dart';
import 'package:flutter_booking_servis/network/base_url.dart';
import 'package:http/http.dart' as http;

class BannerController extends ChangeNotifier {
  List<BannerModel> _banner;
  List<BannerModel> get listBanner => _banner;

  set listBanner(List<BannerModel> val){
    _banner = val;
    notifyListeners();
  }
  
  // Fetch List Banner
  Future<List<BannerModel>> fetchBanner() async{
    final response = await http.get(Uri.parse(Api.baseUrl+"list_banner"));

    List res = jsonDecode(response.body);
    List<BannerModel> data = [];

    for(var i=0; i<res.length;i++){
      var booking = BannerModel.fromJson(res[i]);
      data.add(booking);
    }
    listBanner = data;
    return listBanner;
  }
  // Fetch List Banner
}
