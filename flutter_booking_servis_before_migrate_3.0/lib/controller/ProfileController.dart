import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_booking_servis/model/ProfileModel.dart';
import 'package:flutter_booking_servis/network/base_url.dart';
import 'package:http/http.dart' as http;

class ProfileController extends ChangeNotifier {
  List<ProfileModel> _profile;
  List<ProfileModel> get listprofile => _profile;

  set listprofile(List<ProfileModel> val) {
    _profile = val;
    notifyListeners();
  }

  // Fetch Profile by id_customer
  Future<List<ProfileModel>> fetchProfile(String id_customer) async {
    final response = await http.post(
      Uri.parse(Api.baseUrl + "get_customer_by_id"),
      headers: {"Content-type": "application/x-www-form-urlencoded"},
      body: {
        'id_customer':id_customer
      }
    );

    List res = jsonDecode(response.body);
    List<ProfileModel> data = [];

    for (var i = 0; i < res.length; i++) {
      var profile = ProfileModel.fromJson(res[i]);
      data.add(profile);
    }
    listprofile = data;
    return listprofile;
  }
  // Fetch Profile by id_customer
  
  // Update Profile
  Future<String> updateProfile(String nama, String no_hp,
      String email, String username, String password, String alamat, String id_customer) async {

        // print(nama+""+no_hp+""+email+""+username+""+password+""+alamat+""+id_customer);

    final response = await http.post(
      Uri.parse(Api.baseUrl + "update_profile"),
      headers: {"Content-type": "application/x-www-form-urlencoded"},
      body: {
        'nama': nama,
        'no_hp': no_hp,
        'email': email,
        'username': username,
        'password': password,
        'alamat':alamat,
        'id_customer':id_customer
      },
    );

    // print(response);

    if(response.statusCode == 200){
      // print('sukses nambah data');
      // return response.statusCode;
      return "ok";
    }else{
      throw Exception('gagal booking');
    }
  }
  // Update Profile

}
