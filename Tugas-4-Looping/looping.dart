
import 'dart:io';

void main(List<String> args) {
  // No 1
  var x = 1;
  var y = 20;
  var jawaban1 = "";

  jawaban1 = 'LOOPING PERTAMA \n';
  print(jawaban1);
  while(x<=20){
    if(x % 2 == 0)
    {
      print(x.toString() +' - I will become web developer \n');
    }
    x++;
  }

  jawaban1 = "LOOPING KEDUA";
  print(jawaban1);
  while(y >= 2){
    if(y % 2 == 0)
    {
      print(y.toString() +' - I will become web developer \n');
    }
    y--;
  }
  // No 1

  // No 2
  for(var p=1;p<=20;p++)
  {
    if(p % 2 == 0)
    {
      print(p.toString()+" - Berkualitas \n");
    }
    else
    {
      if(p % 3 == 0 && p % 1 == 0)
      {
          print(p.toString()+" - I Love Coding \n");
      }
      else
      {
        print(p.toString()+" - Santai \n");
      }
    }
  }
  // No 2

  // No 3
  for (int i = 0; i<4; i++) {
   stdout.write("*");
  for (int j= 0; j<8; j++) {
   stdout.write("*");
  }
    print(" ");
  }  
  // No 3

  // No 4
  for(int i=1; i<=7;i++)
  {
    for(int j = 1; j<=7; j++)
    {
      if(j<=i){
        stdout.write("*");
      }else{
        stdout.write(" ");
      }
    }
    print(" ");
  }
  // No 4 
}