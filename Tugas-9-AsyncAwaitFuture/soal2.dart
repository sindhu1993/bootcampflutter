void main(List<String> args) {
  var message = "Life is never flat";
  delayedPrint(3, message).then((value){
    print(value);
  });
}

Future delayedPrint(int seconds, String message) async {
  final duration = Duration(seconds: seconds);
  return Future.delayed(duration).then((value) => message);    
}