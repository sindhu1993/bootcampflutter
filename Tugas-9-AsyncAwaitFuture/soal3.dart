void main(List<String> args) async{
  
  print(await readySing());
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> readySing() async {
    String x = "Ready. Sing";
    return await Future.delayed(Duration(seconds: 5),()=>(x));
}

Future<String> line() async {
  String x = "Pernahkah kau merasa";
   return await Future.delayed(Duration(seconds: 4),()=>(x));
    // print('');
}

Future<String> line2() async {
 String x = "Pernahkah kau merasa";
   return await Future.delayed(Duration(seconds: 3),()=>(x));
}

Future<String> line3() async {
    String x = "Pernahkah kau merasa";
    return await Future.delayed(Duration(seconds: 2),()=>(x));
}

Future<String> line4() async {
   String x = "Hatimu hampa, pernahkah kau merasa hati mu kosong...";
    return await Future.delayed(Duration(seconds: 1),()=>(x));
    // print('');
}