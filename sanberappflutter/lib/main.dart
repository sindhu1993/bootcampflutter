import 'package:flutter/material.dart';
import 'package:sanberappflutter/Quiz3/LoginScreen.dart';
import 'package:sanberappflutter/Quiz3/MainApp.dart';
import 'package:sanberappflutter/Tugas/Tugas12/Telegram.dart';
import 'package:sanberappflutter/Tugas/Tugas13/HomeScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas14/Get_data.dart';
import 'package:sanberappflutter/Tugas/Tugas15/HomeScreenx.dart';
import 'package:sanberappflutter/Tugas/Tugas15/LoginScreenx.dart';
import 'package:sanberappflutter/Tugas/Tugas15/MasterMenu.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: HomeScreen(),
      debugShowCheckedModeBanner: false,
      // home: LoginScreen(),
      // home: GetDataApi(),
      // home: LoginScreenx(),
      home: LoginScreen(),
    );
  }
}
