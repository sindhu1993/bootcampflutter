import 'package:flutter/material.dart';

class DrawwerScreen extends StatefulWidget {
  @override
  _DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawwerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text('Albertus Sindhu Adhi Kusuma'),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/img/albert.jpg"),
            ),
            accountEmail: Text('albertussindhu15@gmail.com'),
          ),
          DrawerListTile(
            iconData: Icons.group,
            title: 'NewGroup',
            onTitlePressed: (){},
          ),
          DrawerListTile(
            iconData: Icons.lock,
            title: 'New Secret Group',
            onTitlePressed: (){},
          ),
          DrawerListTile(
            iconData: Icons.notifications,
            title: 'New Chanel chat',
            onTitlePressed: (){},
          ),
          DrawerListTile(
            iconData: Icons.contacts,
            title: 'contacts',
            onTitlePressed: (){},
          ),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: 'Saved Message',
            onTitlePressed: (){},
          ),
          DrawerListTile(
            iconData: Icons.phone,
            title: 'call',
            onTitlePressed: (){},
          )
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTitlePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTitlePressed}):super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTitlePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(title,style: TextStyle(fontSize: 16),),
    );
  }
}