import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8),
          child: Center(
            child: Column(
              children: [
                Text(
                  'Sanber Flutter',
                  style: TextStyle(fontSize: 20, color: Colors.indigo),
                ),
                SizedBox(
                  height: 10,
                ),
                Image.asset('assets/img/flutter.png'),
                SizedBox(height: 10),
                TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      hintText: 'Username'),
                ),
                SizedBox(
                  height: 5,
                ),
                TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      hintText: 'Password'),
                ),
                SizedBox(
                  height: 5,
                ),
                Center(
                  child: Text(
                    'Forgot Password',
                    style: TextStyle(color: Colors.lightBlue[300]),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                  child: Text('Login'),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.teal,
                    onPrimary: Colors.white,
                    onSurface: Colors.grey,
                  ),
                  onPressed: () {},
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Does not have account'),
                    Text(
                      'Sign In',
                      style: TextStyle(color: Colors.orange, fontSize: 16),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
