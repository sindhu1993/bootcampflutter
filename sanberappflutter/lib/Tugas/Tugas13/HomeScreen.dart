import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 60,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
                IconButton(
                    icon: Icon(Icons.add_shopping_cart_sharp), onPressed: () {})
              ],
            ),
            SizedBox(
              height: 37,
            ),
            Text.rich(
              TextSpan(
                  text: 'Welcome',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.indigo),
                  children: [
                    TextSpan(
                      text: 'Albert',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.indigo[200]),
                    )
                  ]),
              style: TextStyle(fontSize: 50),
            ),
            SizedBox(
              height: 30,
            ),
            TextField(
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search, size: 18),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)),
                  hintText: 'Search'),
            ),
            SizedBox(
              height: 80,
            ),
            Text(
              'Recommended Place',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
            ),
            SizedBox(height: 10),
            SizedBox(
              height: 300,
              child: GridView.count(
                crossAxisCount: 2,
                children: [
                  for(var place in places)
                  Image.asset('assets/img/$place.png')
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}


final places = ['Monas','Roma','Berlin','Tokyo'];