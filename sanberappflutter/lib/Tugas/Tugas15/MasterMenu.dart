import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas15/AccountScreeen.dart';
import 'package:sanberappflutter/Tugas/Tugas15/HomeScreenx.dart';
import 'package:sanberappflutter/Tugas/Tugas15/SearchScreen.dart';

class MasterMenu extends StatefulWidget {
  @override
  _MasterMenuState createState() => _MasterMenuState();
}

class _MasterMenuState extends State<MasterMenu> {
  int selectIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Offstage(
            offstage: selectIndex != 0,
            child: TickerMode(
              enabled: selectIndex == 0,
              child: HomeScreenx(),
            ),
          ),
          Offstage(
            offstage: selectIndex != 1,
            child: TickerMode(
              enabled: selectIndex == 1,
              child: SearchScreen(),
            ),
          ),
          Offstage(
            offstage: selectIndex != 2,
            child: TickerMode(
              enabled: selectIndex == 2,
              child: AccountScreen(),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              InkWell(
                onTap: () {
                  setState(() {
                    selectIndex = 0;
                    print(selectIndex);
                  });
                },
                child: Column(
                  children: [
                    SizedBox(height: 5,),
                    Icon(
                      Icons.home,
                      color: Colors.white,
                    ),
                    Text('Home',style: TextStyle(color: Colors.white),)
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    selectIndex = 1;
                    print(selectIndex);
                  });
                },
                child: Column(
                  children: [
                    SizedBox(height: 5,),
                    Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                    Text('Search',style: TextStyle(color: Colors.white),)
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    selectIndex = 2;
                    print(selectIndex);
                  });
                },
                child: Column(
                  children: [
                    SizedBox(height: 5,),
                    Icon(
                      Icons.people,
                      color: Colors.white,
                    ),
                    Text('Account',style: TextStyle(color: Colors.white),)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
