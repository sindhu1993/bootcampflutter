import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas15/SearchScreen.dart';

class HomeScreenx extends StatefulWidget {
  @override
  _HomeScreenxState createState() => _HomeScreenxState();
}

class _HomeScreenxState extends State<HomeScreenx> {
  int selectIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              accountName: Text('Albertus Sindhu Adhi Kusuma'),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage("img/albert.jpg.jpg"),
              ),
              accountEmail: Text('albertussindhu15@gmail.com'),
            ),
            DrawerListTile(
              iconData: Icons.group,
              title: 'NewGroup',
              onTitlePressed: () {},
            ),
            DrawerListTile(
              iconData: Icons.lock,
              title: 'New Secret Group',
              onTitlePressed: () {},
            ),
            DrawerListTile(
              iconData: Icons.notifications,
              title: 'New Chanel chat',
              onTitlePressed: () {},
            ),
            DrawerListTile(
              iconData: Icons.contacts,
              title: 'contacts',
              onTitlePressed: () {},
            ),
            DrawerListTile(
              iconData: Icons.bookmark_border,
              title: 'Saved Message',
              onTitlePressed: () {},
            ),
            DrawerListTile(
              iconData: Icons.phone,
              title: 'call',
              onTitlePressed: () {},
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
                  IconButton(icon: Icon(Icons.extension), onPressed: () {})
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Text.rich(
                TextSpan(
                    text: 'Welcome',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.indigo),
                    children: [
                      TextSpan(
                        text: 'Albert',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.indigo[200]),
                      )
                    ]),
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 30,
              ),
              TextField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search, size: 18),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)),
                    hintText: 'Search'),
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                'Recommended Place',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
              ),
              SizedBox(
                height: 10,
                width: 10,
              ),
              SizedBox(
                height: 300,
                child: GridView.count(
                  crossAxisCount: 2,
                  children: [
                    for (var place in places)
                      SizedBox(
                        width: 10,
                        child: Image.asset('assets/img/$place.png'),
                      )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTitlePressed;

  const DrawerListTile(
      {Key key, this.iconData, this.title, this.onTitlePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTitlePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}

final places = ['Monas', 'Roma', 'Berlin', 'Tokyo'];
